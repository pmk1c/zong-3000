import { SongDefinition } from '../../../src';
import lead from './lead';

const song: SongDefinition = {
  tempo: 140,
  resolution: 1 / 8,
  timing: 'swing',
  tracks: [
    lead,
  ],
  drums: ['h', 0, 's', 0, 's', 's'],
};

export default song;
