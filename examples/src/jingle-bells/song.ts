import lead from './lead';
import alt from './alt';
import bass from './bass';
import { SongDefinition } from '../../../src';

const song: SongDefinition = {
  tempo: 135,
  resolution: 1 / 8,
  timing: 'swing',
  tracks: [
    lead,
    alt,
    bass,
  ],
  drums: ['h', 'h', 's', 'h', 'h', 'h', 's', 'h',
          'h', 'h', 's', 'h', 'h', 'h', 'h', 'h'],
};

export default song;
