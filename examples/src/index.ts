import { Song, Timing } from '../../src';
import happyBirthday from './happy-birthday/song';
import jingleBells from './jingle-bells/song';
import noDrums from './no-drums';
import letItSnow from './let-it-snow/song';
// import JSONEditor, { JSONEditorMode } from 'jsoneditor';
// import jsonSchema from '../../public/schema.json';
// import jingleBellsSimpleJSON from './jingle-bells-simple';

// import 'jsoneditor/dist/jsoneditor.css';

const AudioContext = ((<any>window).AudioContext || (<any>window).webkitAudioContext);
const context: AudioContext = new AudioContext();
const gainNode = context.createGain();
gainNode.gain.value = 0.3;
gainNode.connect(context.destination);
const songs = [jingleBells, noDrums, letItSnow, happyBirthday].map(s => new Song(s, gainNode));

let currentSong = 0;

// const songInput = document.querySelector('[data-song-definition]') as HTMLElement;
// let jsonEditor: JSONEditor;
// if (songInput) {
//   const options = {
//     mode: "code" as JSONEditorMode,
//     mainMenuBar: false,
//     schema: jsonSchema,
//   };
//   jsonEditor = new JSONEditor(songInput, options);
//   jsonEditor.setText(jingleBellsSimpleJSON);
// }

// const updateSong = document.querySelector('[data-update-song]');
// if(updateSong && songInput) {
//   updateSong.addEventListener('click', function() {
//     songs[currentSong].stop();
//     songs = [new Song(jsonEditor.get(), gainNode)];
//     songs[currentSong].play();
//   });
// }

const playPause = document.querySelector('[data-playpause]');
if (playPause) {
  playPause.addEventListener('click', function () {
    if (songs[currentSong].isPlaying()) {
      songs[currentSong].pause();
    } else {
      songs[currentSong].play();
    }
  });
}

const stop = document.querySelector('[data-stop]');
if (stop) {
  stop.addEventListener('click', function () {
    songs[currentSong].stop();
  });
}

const tempo = (<HTMLInputElement>document.querySelector('input[data-tempo]'));
tempo.value = songs[currentSong].tempo.toString();
if (tempo) {
  tempo.addEventListener('change', function () {
    songs[currentSong].tempo = Number.parseInt(tempo.value);
  })
}

const timing = (<HTMLSelectElement>document.querySelector('select[data-timing]'));
timing.value = songs[currentSong].timing;
if (timing) {
  timing.addEventListener('change', function () {
    songs[currentSong].timing = (<Timing>timing.value);
  })
}

const next = document.querySelector('[data-next]');
if (next) {
  next.addEventListener('click', function () {
    if (songs[currentSong].isPlaying()) {
      songs[currentSong].stop();
    }

    currentSong++;
    if (currentSong >= songs.length) {
      currentSong = 0;
    }

    songs[currentSong].play();
  });
}
