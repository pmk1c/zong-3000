import { SongDefinition } from "../../src";

const song: SongDefinition = {
  tempo: 140,
  resolution: 1 / 8,
  timing: 'swing',
  tracks: [{
    sound: 1,
    notes: [
      [['c', 3], 1 / 4],
      [['d', 3], 1 / 4],
      [['e', 3], 1 / 4],
      [['f', 3], 1 / 4],
      [['g', 3], 1 / 4],
      [['a', 3], 1 / 4],
      [['b', 3], 1 / 4],
      [['c', 4], 1 / 4],
    ],
  }],
  drums: [],
};

export default song;
