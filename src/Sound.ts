import Position from "./Position";
import { Timing } from "./types";

export default abstract class Sound {
  position: number;
  constructor(position: number) {
    this.position = position;
  }

  getStartTime(beatsPerMinute: number, zeroOffsetTime: number, timing: Timing) {
    const position = Position.withTiming(this.position, timing)
    return Position.toTime(position, beatsPerMinute, zeroOffsetTime);
  }
}
