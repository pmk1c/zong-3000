import Sound from "./Sound";
import { DrumKickDefinition, Timing } from "./types";

const KICK_LENGTHS = {
  0: 0,
  'h': 0.01,
  's': 0.1,
}

export default class DrumSound extends Sound {
  kick: DrumKickDefinition;
  sink: AudioNode;
  audioBufferSource?: AudioBufferSourceNode;
  noiseSample: AudioBuffer;
  _ended: boolean;
  constructor(kick: DrumKickDefinition, position: number, sink: AudioNode, noiseSample: AudioBuffer) {
    super(position);
    this.kick = kick;
    this.sink = sink;
    this.noiseSample = noiseSample;
    this._ended = false;
  }

  schedule(beatsPerMinute: number, zeroOffsetTime: number, timing: Timing) {
    this.audioBufferSource?.stop();
    this._ended = false;
    this.audioBufferSource = new AudioBufferSourceNode(this.sink.context);
    this.audioBufferSource.buffer = this.noiseSample;
    this.audioBufferSource.loop = true;
    this.audioBufferSource.loopStart = Math.random() * this.noiseSample.duration;
    this.audioBufferSource.connect(this.sink);
    this.audioBufferSource.start(this.getStartTime(beatsPerMinute, zeroOffsetTime, timing));
    this.audioBufferSource.stop(this.getStopTime(beatsPerMinute, zeroOffsetTime, timing));
    const source = this.audioBufferSource;
    source.onended = () => {
      source.disconnect();
      this._ended = true;
    };
  }

  cancel() {
    this.audioBufferSource?.stop();
    this.audioBufferSource = undefined;
  }

  getStopTime(beatsPerMinute: number, zeroOffsetTime: number, timing: Timing) {
    return this.getStartTime(beatsPerMinute, zeroOffsetTime, timing) + KICK_LENGTHS[this.kick];
  }

  get ended() {
    return this._ended;
  }
}
