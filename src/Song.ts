import Track from './Track';
import DrumTrack from './DrumTrack';
import Position from './Position';
import { SongDefinition, Timing } from './types';

const DURATION_TO_SCHEDULE = 1;
const SCHEDULE_DELAY_TIME = 0.1;

export default class Song {
  context: BaseAudioContext;
  private _tempo: number;
  private _timing: Timing;
  resolution: number;
  tracks: Track[];
  drumTrack?: DrumTrack;
  scheduleTimeout: number | undefined;
  zeroOffsetTime: number;
  playedTime: number;
  scheduledUntilPosition: number;
  noiseSample: AudioBuffer;
  constructor(song: SongDefinition, sink: AudioNode) {
    this.context = sink.context;
    this._tempo = song.tempo;
    this._timing = song.timing || 'straight';
    this.resolution = song.resolution || 1 / 8;
    this.tracks = song.tracks.map(track => new Track(this, track, sink));
    if (song.drums) {
      this.drumTrack = new DrumTrack(this, song.drums, sink);
    }
    this.zeroOffsetTime = 0;
    this.playedTime = 0;
    this.scheduledUntilPosition = 0;
    this.noiseSample = this.context.createBuffer(2, this.context.sampleRate * 3, this.context.sampleRate);
    this.registerNoiseSample();
  }

  play() {
    this.zeroOffsetTime = this.context.currentTime - this.playedTime + SCHEDULE_DELAY_TIME;
    this.schedule();
  }

  reschedule() {
    this.scheduledUntilPosition = 0;
    this.drumTrack?.cancel();
    this.schedule(true)
  }

  schedule(once = false) {
    const nextTimeToSchedule = this.context.currentTime + SCHEDULE_DELAY_TIME;
    const fromPosition = Math.max(this.scheduledUntilPosition, Position.fromTime(nextTimeToSchedule, this.tempo, this.zeroOffsetTime));
    const toPosition = Position.fromTime(nextTimeToSchedule + DURATION_TO_SCHEDULE, this.tempo, this.zeroOffsetTime);
    for (const track of this.tracks) {
      track.reschedule(fromPosition, toPosition, this.tempo, this.zeroOffsetTime, this.timing);
    }
    this.drumTrack?.schedule(fromPosition, toPosition, this.tempo, this.zeroOffsetTime, this.timing);

    if (!once) {
      this.scheduleTimeout = window.setTimeout(this.schedule.bind(this), 200);
    }
    this.scheduledUntilPosition = toPosition;

    if (this.currentPosition > this.length) {
      this.stop();
    }
  }

  get length() {
    return Math.max(...this.tracks.map(track => track.length));
  }

  get tempo() {
    return this._tempo;
  }

  set tempo(newTempo: number) {
    const currentTime = this.context.currentTime;
    const currentPosition = Position.fromTime(currentTime, this.tempo, this.zeroOffsetTime);

    const newCurrentTime = Position.toTime(currentPosition, newTempo, this.zeroOffsetTime);
    const offset = currentTime - newCurrentTime;

    this.zeroOffsetTime += offset;
    this._tempo = newTempo;

    this.reschedule();
  }

  get timing() {
    return this._timing;
  }

  set timing(newTiming: Timing) {
    this._timing = newTiming;
    this.reschedule();
  }

  stop() {
    this.cancel();
    this.playedTime = 0;
  }

  pause() {
    this.cancel();
    this.playedTime = this.context.currentTime - this.zeroOffsetTime;
  }

  isPlaying() {
    return !!this.scheduleTimeout
  }

  get currentPosition() {
    return Position.fromTime(this.context.currentTime, this.tempo, this.zeroOffsetTime);
  }

  private cancel() {
    clearTimeout(this.scheduleTimeout);
    this.scheduleTimeout = undefined;
    this.scheduledUntilPosition = 0;
    this.drumTrack?.cancel();
    this.tracks.forEach(t => t.cancel());
  }

  private registerNoiseSample() {
    for (let channel = 0; channel < this.noiseSample.numberOfChannels; channel++) {
      const nowBuffering = this.noiseSample.getChannelData(channel);
      for (let i = 0; i < this.noiseSample.length; i++) {
        nowBuffering[i] = Math.random() * 2 - 1;
      }
    }
  }
}
