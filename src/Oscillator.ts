// See: https://developer.mozilla.org/en-US/docs/Web/API/WaveShaperNode/curve#Example
function makeDistortionCurve(amount: number) {
  var k = typeof amount === 'number' ? amount : 50,
    n_samples = 44100,
    curve = new Float32Array(n_samples),
    deg = Math.PI / 180,
    i = 0,
    x;
  for (; i < n_samples; ++i) {
    x = i * 2 / n_samples - 1;
    curve[i] = (3 + k) * x * 20 * deg / (Math.PI + k * Math.abs(x));
  }
  return curve;
};

export default class Oscillator {
  context: BaseAudioContext;
  distortion: WaveShaperNode;
  oscillator: OscillatorNode;
  constructor(context: BaseAudioContext, wave: OscillatorType) {
    this.context = context;

    this.distortion = context.createWaveShaper();
    this.distortion.curve = makeDistortionCurve(50);
    this.distortion.oversample = '4x';

    this.oscillator = context.createOscillator();
    this.oscillator.connect(this.distortion);
    this.oscillator.type = wave;
    this.oscillator.frequency.value = 0;
    this.oscillator.onended = () => {
      this.oscillator.disconnect();
      this.distortion.disconnect();
    };
  }

  setFrequency(frequency: number, time?: number) {
    this.oscillator.frequency.setValueAtTime(frequency, time || this.context.currentTime);
  }

  start(time: number) {
    this.oscillator.start(time);
  }

  stop(time?: number) {
    this.oscillator.stop(time);
  }

  cancel(time: number) {
    this.oscillator.frequency.cancelScheduledValues(time);
    this.oscillator.frequency.setValueAtTime(0, time);
  }

  connect(node: AudioNode) {
    this.distortion.connect(node);
  }
}
