export type NoteName = 'c' | 'cs' | 'c#' | 'd' | 'ds' |'d#' | 'e' | 'f' | 'fs' | 'f#' | 'g' | 'gs' |'g#' | 'a' | 'as' |'a#' | 'b' | 'h'

export type NoteValue = NoteValueEnum | number;

export enum NoteValueEnum {
  whole = "1",
  half = "1/2",
  quarter = "1/4",
  eighth = "1/8",
  sixteenth = "1/16",
}

export type Timing = 'straight' | 'swing';

export enum Octave {
  subsubcontra = -1,
  subcontra = 0,
  contra = 1,
  great = 2,
  small = 3,
  onelined = 4,
  twolined = 5,
  threelined = 6,
  fourlined = 7,
  fivelined = 8,
  sixlined = 9,
}

export type Rest = 'r';

export interface SongDefinition {
  readonly tempo: number;
  readonly resolution?: number;
  readonly timing: Timing,
  readonly tracks: TrackDefinition[],
  readonly drums?: DrumTrackDefinition,
}

export interface TrackDefinition {
  readonly sound: SoundDefinition,
  readonly notes: NoteDefintion[],
}

export enum SoundDefinition {
  sine = 1,
  square = 2,
  triangle = 3,
  sawtooth = 4,
}

export interface NoteDefintion {
  readonly 0: NoteTypeDefinition;
  readonly 1: NoteValue;
}

export type NoteTypeDefinition = Rest | NoteKeyDefinition;

export interface NoteKeyDefinition {
  readonly 0: NoteName,
  readonly 1: Octave,
}

export type DrumTrackDefinition = DrumKickDefinition[];

export type DrumKickDefinition = 'h' | 's' | 0;
