import Song from './Song';
import Sound from './DrumSound';
import { DrumKickDefinition, DrumTrackDefinition, Timing } from './types';

export default class DrumTrack {
  song: Song;
  kicks: DrumKickDefinition[];
  scheduledSounds: Sound[];
  sink: AudioNode;
  constructor(song: Song, kicks: DrumTrackDefinition, sink: AudioNode) {
    this.song = song;
    this.kicks = kicks;
    this.sink = sink;
    this.scheduledSounds = [];
  }

  schedule(fromPosition: number, toPosition: number, beatsPerMinute: number, zeroOffsetTime: number, timing: Timing) {
    const sounds = this.getSoundsBetween(fromPosition, toPosition);
    for (const sound of sounds) {
      sound.schedule(beatsPerMinute, zeroOffsetTime, timing);
    }
    if (sounds.length > 0) {
      this.scheduledSounds = [...this.scheduledSounds.filter(sound => !sound.ended), ...sounds];
    }
  }

  cancel() {
    this.scheduledSounds.forEach(sound => sound.cancel());
    this.scheduledSounds = [];
  }

  private getSoundsBetween(fromPosition: number, toPosition: number) {
    const fromTick = Math.ceil(fromPosition / this.song.resolution);
    const toTick = Math.ceil(toPosition / this.song.resolution);

    const sounds: Sound[] = [];
    for (let tick = fromTick; tick < toTick; tick++) {
      const kick = this.kicks[tick % this.kicks.length];
      if (kick) {
        const sound = new Sound(kick, tick * this.song.resolution, this.sink, this.song.noiseSample);
        sounds.push(sound);
      }
    }

    return sounds;
  }
}
