import Song from './Song';
import Sound, { NOTE_VALUES } from './NoteSound';
import { NoteDefintion, NoteValue, SoundDefinition, Timing, TrackDefinition } from './types';

const WAVE_SOUNDS = {
  [SoundDefinition.sine]: "sine" as OscillatorType,
  [SoundDefinition.square]: "square" as OscillatorType,
  [SoundDefinition.triangle]: "triangle" as OscillatorType,
  [SoundDefinition.sawtooth]: "sawtooth" as OscillatorType,
}
export default class Track {
  song: Song;
  sounds: Sound[];
  wave: OscillatorType;
  sink: AudioNode;
  constructor(song: Song, track: TrackDefinition, sink: AudioNode) {
    this.song = song;
    this.wave = WAVE_SOUNDS[track.sound];
    this.sink = sink;
    this.sounds = this.createSounds(track.notes);
  }

  reschedule(fromPosition: number, toPosition: number, beatsPerMinute: number, zeroOffsetTime: number, timing: Timing) {
    const soundsToReschedule = this.getSoundsBetween(fromPosition, toPosition, timing);
    const soundsToCancel = this.getSoundsAfter(toPosition);
    for (const sound of soundsToReschedule) {
      sound.reschedule(beatsPerMinute, zeroOffsetTime, timing, fromPosition);
    }
    for (const sound of soundsToCancel) {
      sound.cancel();
    }
  }

  cancel() {
    this.sounds.forEach(sound => sound.cancel());
  }

  get length() {
    const lastSound = this.sounds[this.sounds.length - 1];
    return lastSound.position + lastSound.noteValue;
  }

  private createSounds(notes: NoteDefintion[]) {
    const sounds = [];
    let position = 0;
    for (const note of notes) {
      if (note[0] != 'r') {
        sounds.push(new Sound(note, position, this.wave, this.sink));
      }
      position += this.getNoteValue(note[1]);
    }

    return sounds;
  }

  private getSoundsBetween(fromPosition: number, toPosition: number, timing: Timing) {
    return this.sounds.filter(sound => sound.isPlaying(fromPosition, timing) || sound.position >= fromPosition && sound.position < toPosition);
  }

  private getSoundsAfter(position: number) {
    return this.sounds.filter(sound => sound.position >= position);
  }

  private getNoteValue(noteValue: NoteValue) {
    if (typeof noteValue === "number") {
      return noteValue;
    } else {
      return NOTE_VALUES[noteValue];
    }
  }
}
