import { Timing } from "./types";

const SWING_OFFSET = (1 / 4) * (2 / 3) - (1 / 8)

export default class Position {
  static fromTime(time: number, beatsPerMinute: number, zeroOffsetTime: number) {
    const positionAsTimeInSeconds = time - zeroOffsetTime;
    const positionAsTimeInMinutes = positionAsTimeInSeconds / 60;
    const positionAsBeats = positionAsTimeInMinutes * beatsPerMinute;
    return positionAsBeats / 4;
  }

  static toTime(position: number, beatsPerMinute: number, zeroOffsetTime: number) {
    const positionAsBeats = position * 4;
    const positionAsTimeInMinutes = positionAsBeats / beatsPerMinute;
    const positionAsTimeInSeconds = positionAsTimeInMinutes * 60;
    return positionAsTimeInSeconds + zeroOffsetTime;
  }

  static withTiming(position: number, timing: Timing) {
    if (timing === 'straight') {
      return position;
    }

    const eighthIndex = position / (1 / 8);

    if (!Number.isSafeInteger(eighthIndex)) {
      console.error("Unable to swing if position is not a multitude of an eighth note.")
      return position;
    }

    const isLayedBack = (eighthIndex % 2) === 1;
    return isLayedBack ? position + SWING_OFFSET : position;
  }
}
