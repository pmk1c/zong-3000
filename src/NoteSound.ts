import Oscillator from './Oscillator';
import Position from "./Position";
import Sound from "./Sound";
import { NoteValueEnum, NoteDefintion, Timing } from './types';

export const NOTE_VALUES = {
  [NoteValueEnum.whole]: 1,
  [NoteValueEnum.half]: 1/2,
  [NoteValueEnum.quarter]: 1/4,
  [NoteValueEnum.eighth]: 1/8,
  [NoteValueEnum.sixteenth]: 1/16,
}

const NOTES = {
  c: 0,
  cs: 1,
  "c#": 1,
  d: 2,
  ds: 3,
  "d#": 3,
  e: 4,
  f: 5,
  fs: 6,
  "f#": 6,
  g: 7,
  gs: 8,
  "g#": 8,
  a: 9,
  as: 10,
  "a#": 10,
  b: 11,
  h: 11,
}
const FIXED_NOTE: ['a', 4] = ['a', 4];
const FIXED_FREQUENCY = 440;
const EQUAL_TEMPERED_FACTOR = Math.pow(2, 1 / 12);

const STOP_GAP = 0.01;

export default class NoteSound extends Sound {
  note: NoteDefintion;
  sink: AudioNode;
  wave: OscillatorType;
  oscillator?: Oscillator;
  constructor(note: NoteDefintion, position: number, wave: OscillatorType, sink: AudioNode) {
    super(position);
    this.note = note;
    this.sink = sink;
    this.wave = wave;
  }

  reschedule(beatsPerMinute: number, zeroOffsetTime: number, timing: Timing, fromPosition: Number) {
    if (!this.isPlaying(fromPosition, timing) || !this.oscillator) {
      this.oscillator?.stop();
      this.oscillator = new Oscillator(this.sink.context, this.wave);
      this.oscillator.setFrequency(this.frequency);
      this.oscillator.connect(this.sink);
      this.oscillator.start(this.getStartTime(beatsPerMinute, zeroOffsetTime, timing));
    }
    this.oscillator?.stop(this.getStopTime(beatsPerMinute, zeroOffsetTime, timing));
  }

  isPlaying(position: Number, timing: Timing) {
    return this.position <= position && this.getStopPosition(timing) >= position;
  }

  cancel() {
    this.oscillator?.stop();
    this.oscillator = undefined;
  }

  getStopPosition(timing: Timing) {
    return Position.withTiming(this.position + this.noteValue, timing);
  }

  getStopTime(beatsPerMinute: number, zeroOffsetTime: number, timing: Timing) {
    const stopPosition = this.getStopPosition(timing);
    const stopTime = Position.toTime(stopPosition, beatsPerMinute, zeroOffsetTime);
    return stopTime - STOP_GAP;
  }

  get frequency() {
    if (this.note[0] === 'r') {
      return 0;
    }

    return FIXED_FREQUENCY * Math.pow(EQUAL_TEMPERED_FACTOR, this.halfStepsFromFixedNote);
  }

  private get halfStepsFromFixedNote() {
    if (this.note[0] === 'r') {
      return 0;
    }

    const halfSteps = NOTES[this.note[0][0]] - NOTES[FIXED_NOTE[0]];
    const octaveHalfSteps = (this.note[0][1] - FIXED_NOTE[1]) * 12;
    return halfSteps + octaveHalfSteps;
  }

  get noteValue() {
    const noteValue = this.note[1];
    if (typeof noteValue === "number") {
      return noteValue;
    } else {
      return NOTE_VALUES[noteValue];
    }
  }
}
